
inlets = 2;
outlets = 6;

post("barData.js saved\n");

// message types

BAR_GAIN = "BAR_GAIN";
NEGATIVE_VELOCITY_OFFSET = "NEGATIVE_VELOCITY_OFFSET";
TARE = "TARE";
BAR_DOWN_THRESHOLD = "BAR_DOWN_THRESHOLD";
BAR_UP_THRESHOLD = "BAR_UP_THRESHOLD";
RAW_VELOCITY = "RAW_VELOCITY";


function Bar() {
	this.lowSamples_untared = [];
	this.highSamples_untared = [];
	this.lowRecordedVal_untared = 999;
	this.highRecordedVal_untared = 999;



	this.tare = 999;
	this.gain = 999;
	this.negativeVelocityOffset = 999;
	this.barDownThreshold = 999;
	this.barUpThreshold = 999;
	this.lastRecordedVelocity = 999;
}

barObjects = [];
for (var i=0; i<43; i++) barObjects.push(new Bar());

selectedBarNum = 0;

function msg_fromMarimba() {
	parseMessage(arguments);
}

function parseMessage(msg) {
	var messageType = msg[0];
	var barNum = msg[1];
	var value = msg[2];
	var barObject = barObjects[barNum];
	switch (messageType) {
		case RAW_VELOCITY:
			outlet(0, barNum, value); // handle the message in learnMode.js
			break;
		case BAR_GAIN:
			//post("RCV gain>", barNum, value, "\n");
			barObject.gain = value;

			messnamed("requestUnitData_barGainReceived", "bang"); //todo: rewrite
			outlet(1, barNum, barObject.gain);
			break;
		case NEGATIVE_VELOCITY_OFFSET:
			//post("RCV negativeVelocityOffset>", barNum, value, "\n");
			barObject.negativeVelocityOffset = value;

			messnamed("requestUnitData_negativeVelocityOffsetReceived", "bang"); //todo: rewrite
			outlet(2, barNum, barObject.negativeVelocityOffset);
			break;
		case TARE: 
			//post("RCV tare>", barNum, value, "\n");
			barObject.tare = value;
			outlet(3, barNum, barObject.tare);
			break;
		case BAR_DOWN_THRESHOLD:
			//post("RCV barDownThreshold>", barNum, value, "\n");
			barObject.barDownThreshold = value;

			messnamed("requestUnitData_barDownThreshReceived", "bang"); //todo: rewrite
			outlet(4, barNum, barObject.barDownThreshold);
			break;
		case BAR_UP_THRESHOLD:
			//post("RCV barUpThreshold", barNum, value, "\n");
			barObject.barUpThreshold = value;
			messnamed("requestUnitData_barUpThreshReceived", "bang"); //todo: rewrite
			outlet(5, barNum, barObject.barUpThreshold);
			break;

		default:
			break;
	}
}



function setSelectedBarNum(barNum) {
	selectedBarNum = barNum;
	messnamed("barNumSet", selectedBarNum); // don't force output
	messnamed("highlightBarPanel", selectedBarNum);
	var selectedBar = barObjects[selectedBarNum];
	messnamed("selectedBarGain", selectedBar.gain);
	messnamed("selectedBarNegativeVelocityOffset", selectedBar.negativeVelocityOffset);
	messnamed("selectedBarBarDownThresh", selectedBar.barDownThreshold);
	messnamed("selectedBarBarUpThresh", selectedBar.barUpThreshold);
	messnamed("velocitySamples", 0, [0]);
	messnamed("velocitySamples", 1, [0]);
	messnamed("velocitySamples", 0, selectedBar.lowSamples_untared);
	messnamed("velocitySamples", 1, selectedBar.highSamples_untared);
}

function getTimeStamp() {
	var date = new Date();
	var seconds = date.getTime();
	var minutes = date.getMinutes();
	var hour = date.getHours();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	var timestamp = "_" + year + "_" + month + "_" + day + "_" + hour + "_" + minutes;
	return timestamp;
}

function makeMinMaxTareGainNegativeOffsetCsvFunctionCall(filename, folderpath) {
	var matrix = [["avg untared low", "avg untared high", "tare", "gain", "negative velocity offset", "BarDownThreshold", "BarUpThreshold"]];
	for (var i = 0; i < barObjects.length; i++) {
		var bar = barObjects[i];
		var row = [bar.lowRecordedVal_untared, bar.highRecordedVal_untared, bar.tare, round(bar.gain, 2), bar.negativeVelocityOffset, bar.barDownThreshold, bar.barUpThreshold];
		matrix.push(row);
	}
	var timeStamp = getTimeStamp();
	makeCSV(filename + timeStamp, folderpath, matrix);
}

function setFilename(stringFromDialog) {
	csvFilename = stringFromDialog;
}

/* modfied version of `writeFile` function from nathanscellblock2csv.js in _Resistance_Array_Mapper.maxpat */
function makeCSV(filename, folderpath, matrix) { 
	/* DEBUG: Print out the contents åœof the matrix to the MAX window to make sure everything is right  
	post(matrix[0] + "\n");
	for(j = 0; j <= (matrix[0].length-1); jå++) {
		for(i = 0; i <= (matrix-1); i++ ) {
			post(matrix[i][j], ',');		
		}	
		post('\n');
	}
	*/
	if (folderpath[folderpath.length-1] != "/") {
		folderpath += "/";
	}
	
	var file = new File(folderpath + filename + ".csv", "readwrite", 'TEXT'); // create a new file
	file.eof = 0; // clear the contents in the file

	for (var i=0; i<600; i++) {
		file.writeline(""); // this is probably unecessary but i will hopefully confirm whether or not it is later
	}
	
	file.position = 0;
	
	/* Go through each row and collect the data in each column. After all the data in the row has
	been assigned to the "line" string, write the string to a line with the writeline method */
	for(var i = 0; i < (matrix.length); i++) //base the amount of columns on the length of the row array in the first column (matrix[0])
	{
		var line = ''; // initialize the new line to get it ready for a new row of data, for some reason this is necessary, probably to make line of string type
		for(j = 0; j < (matrix[i].length); j++) {
			// skip the undefined values
			if (matrix[i][j] != undefined) {
				line += matrix[i][j] + ','; //add the value data point to the appropriate position in the matrix
			}
		}
		
		file.writeline(line); //after creating a row of data, write it to the file
	}
	
	post("Finished writing to file " + folderpath + filename + ".csv \n");	
	
	file.close();		
}


function resetVis() {
	barObjects = [];
	for (var i=0; i<43; i++) {
		barObjects[i] = new Bar();
		messnamed("resetNumBoxes", i, 999);
	}
}


