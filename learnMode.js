	
post("learnMode.js saved\n");

inlets = 2;
outlets = 5;

autowatch = true;


///////////////////////////////////////////////////
///////////////////////////////////////////////////
//////////// LEARN MODE ALGORITHM /////////////////
///////////////////////////////////////////////////
///////////////////////////////////////////////////

var lowTargetVal = 30;  // settable from UI
var highTargetVal = 130; // settable from UI


var MAX_GAIN = 2.27;
var MAX_OFFSET = 255;

messnamed("resetTargets", "bang");

/*

function Bar() {
	this.minSamples = [];
	this.maxSamples = [];
	this.min = 999;
	this.max = 999;
	this.tare = 999;
	this.gain = 999;
	this.negativeVelocityOffset = 999;
}

*/





function calculateGain(bar, barNum) {
	var rise = highTargetVal - lowTargetVal; //OUTPUT_MAX and OUTPUT_MIN tuned to solenoid data
	var run = (bar.highRecordedVal_untared) - (bar.lowRecordedVal_untared);
	return min(MAX_GAIN, rise/run); // MAX_GAIN = 2.27
}

function calculateNegativeOffset(bar, barNum) {
	var b = lowTargetVal - bar.gain*(bar.lowRecordedVal_untared - bar.tare);
	
	
	
	var calculatedNegativeOffset = round(-b, 0); // deal with every in positives
	if (calculatedNegativeOffset < 0) {
		calculatedNegativeOffset = 0;
	}

	post("learnmode", barNum, lowTargetVal, "-", bar.gain, "*(", bar.lowRecordedVal_untared, "-", bar.tare, ")=", b, "\n");
	return min(MAX_OFFSET, calculatedNegativeOffset); // MAX_OFFSET = 255
}



///////////////////////////////////////////////////
///////////////////////////////////////////////////
//////////////////// UI CODE //////////////////////
///////////////////////////////////////////////////
///////////////////////////////////////////////////

var currentRecordingVelocitySamples_untared = [];


var NUM_SAMPLES = 5;
var RECORD_MODE_MIN = "RECORD_MODE_MIN";
var RECORD_MODE_MAX = "RECORD_MODE_MAX";
var recordMode = RECORD_MODE_MIN; 
var selectedBarNumGain;
var selectedBarNumOffset;
var recordingGain = 2.27;
var isRecordingRawVelocities = false;


function list() {
	var barNum = arguments[0];
	var velocity = arguments[1];

	if (!isRecordingRawVelocities || barNum != selectedBarNum) {
		return;
	}

	post("LEARNMODE:", barNum, velocity, "\n");

	var barObject = barObjects[barNum];
	currentRecordingVelocitySamples_untared.push(velocity);

	var numHits = currentRecordingVelocitySamples_untared.length;
	messnamed("numHits", numHits + "/" + NUM_SAMPLES);
	messnamed("velocitySamples", recordMode == RECORD_MODE_MAX, currentRecordingVelocitySamples_untared);
	
	if (numHits == NUM_SAMPLES) {

		switch (recordMode) {
			case RECORD_MODE_MIN:
				barObject.lowSamples_untared = lstCopy(currentRecordingVelocitySamples_untared);
				barObject.lowRecordedVal_untared = round(average(barObject.lowSamples_untared), 0);
				outletAveragedLow(barNum); // s minNumBoxes
				break;
				
			case RECORD_MODE_MAX:
				barObject.highSamples_untared = lstCopy(currentRecordingVelocitySamples_untared);
				barObject.highRecordedVal_untared = round(average(barObject.highSamples_untared), 0);
				outletAveragedHigh(barNum); // s minNumBoxes
				break;
			default:
				break;
		}
		
		outletHighLowDifference(barNum)
		currentRecordingVelocitySamples_untared = [];
		messnamed("recordButton", 0);

		if (barObject.highSamples_untared.length == NUM_SAMPLES && barObject.lowSamples_untared.length == NUM_SAMPLES) {
			calculateAndSendGainAndNegativeOffset(barNum);
		}
	}
}

function setLearnModeTargets(lowTargetNumBoxVal, highTargetNumBoxVal) {
	post("targets:", lowTargetNumBoxVal, highTargetNumBoxVal, "\n")
	lowTargetVal = lowTargetNumBoxVal;
	highTargetVal = highTargetNumBoxVal;
}


function toggleRecording(toggleVal) {
	isRecordingRawVelocities = toggleVal == 1;
	var selectedBar = barObjects[selectedBarNum];
	if (isRecordingRawVelocities) {

		// record its current gain and offset in case cancel is hit
		selectedBarNumGain = selectedBar.gain;
		selectedBarNumOffset = selectedBar.negativeVelocityOffset;
		outletBarGain(selectedBarNum, recordingGain);
		outletNegativeOffset(selectedBarNum, 0);
	}
	else {
		currentRecordingVelocitySamples_untared = [];

		// if cancel pressed, restore gain and -offset values
		if (selectedBarNumGain != undefined && selectedBarNumOffset != undefined) {
			outletBarGain(selectedBarNum, selectedBarNumGain)
			outletNegativeOffset(selectedBarNum, selectedBarNumOffset);
		}
	}
}

function setRecordMode(radioGroupVal) {
	switch (radioGroupVal) {
		case 0:
			recordMode = RECORD_MODE_MIN;
			break;
		case 1:
			recordMode = RECORD_MODE_MAX;
			break;
		default:
			break;
	}
}

function calculateAndSendGainAndNegativeOffset(barNum) {
	var barObject = barObjects[barNum];
	barObject.gain = calculateGain(barObject, barNum);
	
	barObject.negativeVelocityOffset = calculateNegativeOffset(barObject, barNum);
	post(barNum, barObject.negativeVelocityOffset, "\n");
	outletBarGain(barNum, barObject.gain);
	outletNegativeOffset(barNum, barObject.negativeVelocityOffset);
}

function uploadRowFromCSV(barNum, low_untared, high_untared, tare) {
	var barObject = barObjects[barNum];
	barObject.lowRecordedVal_untared = low_untared;
	barObject.highRecordedVal_untared = high_untared;
	barObject.tare = tare;
	outletAveragedLow(barNum); 
	outletAveragedHigh(barNum);
	messnamed("bar_tare_fromLearnMode", barNum, barObject.tare); // TODO: change this to outlet
	outletHighLowDifference(barNum);

	
	calculateAndSendGainAndNegativeOffset(barNum);
}

function setRecordingGain(num) {
	recordingGain = num;
}

/// OUTLETS

function outletAveragedLow(barNum) {
	var barObject = barObjects[barNum];
	outlet(2, "low_tared", barObject.lowRecordedVal_untared - barObject.tare); // s averageMinValsTared
	outlet(2, "low_untared", barNum, barObject.lowRecordedVal_untared); // s averageMinValsTared

}

function outletAveragedHigh(barNum) {
	var barObject = barObjects[barNum];
	outlet(2, "high_tared", barNum, barObject.highRecordedVal_untared - barObject.tare); // s averageMinValsTared
	outlet(2, "high_untared", barNum, barObject.highRecordedVal_untared); // s averageMinValsTared
}

function outletHighLowDifference(barNum) {
	var barObject = barObjects[barNum];
	var val = barObject.highRecordedVal_untared - barObject.lowRecordedVal_untared
	outlet(4, barNum, val)
}

function outletBarGain(bar, gain) {
	outlet(1, "sendBarGain", bar, gain);
	outlet(0, "requestBarGain", bar);
}

function outletNegativeOffset(bar, negativeVelocityOffset) {
	outlet(1, "sendNegativeVelocityOffset", bar, negativeVelocityOffset);
	outlet(0, "requestNegativeVelocityOffset", bar);
}


///////////////////////////////////////////////////
///////////////////////////////////////////////////
///////////// BASIC HELPER FUNCTIONS //////////////
///////////////////////////////////////////////////
///////////////////////////////////////////////////

//// BASIC HELPER FUNCTIONS

function average(lst) {
	var sum = 0;	
	for (var i=0; i<lst.length; i++) {
		sum += lst[i];
	}
	return sum/lst.length;
}

function min(x, y) {
	if (x<y) return x;
	else return y;
}

function max(x, y) {
	if (x>y) return x;
	else return y;
}

function lstCopy(lst) {
	var toReturn = [];
	for (var i=0; i<lst.length; i++) {
		toReturn.push(lst[i])
	}
	return toReturn;
}


function round(num, numDecimalPlaces) {
	if (numDecimalPlaces == 0) {
		return Math.floor(num)
	}

	return Math.floor(num * Math.pow(10, numDecimalPlaces+1)) / Math.pow(10, numDecimalPlaces+1); 
}

