inlets = 2;
outlets = 2;

autowatch = true;

var unitCurrentMode;

LEARN_MODE = 4;
NORMAL_MODE = 0;

post("malletStation_sendsAndRequests.js saved\n");

////////////////////////////////////////////////////
///////////////////// SENDS ////////////////////////
////////////////////////////////////////////////////

function sendBarGain(barNum, value) {
	if (unitCurrentMode != LEARN_MODE) {
		setLearnMode();
	}
	outlet(0, "/g/w/barGain", barNum, value*1.000001)
}

function sendNegativeVelocityOffset(barNum, value) {
	if (unitCurrentMode != LEARN_MODE) {
		setLearnMode();
	}
	outlet(0, "/g/w/negativeVelocityOffset", barNum, value)
}

function sendBarDownThreshold(barNum, value) {
	if (unitCurrentMode != NORMAL_MODE) {
		setNormalMode();
	}
	outlet(0, "/g/w/barDownThreshold", barNum, value)
}

function sendBarUpThreshold(barNum, value) {
	if (unitCurrentMode != NORMAL_MODE) {
		setNormalMode();
	}
	outlet(0, "/g/w/barUpThreshold", barNum, value)
}

function setLearnMode() {
	post("setLearnMode()\n")
	unitCurrentMode = LEARN_MODE;
	outlet(0, "/fw/w/mode", LEARN_MODE);
}

function setNormalMode() {
	post("setNormalMode()\n")
	unitCurrentMode = NORMAL_MODE;
	outlet(0, "/fw/w/mode", NORMAL_MODE);
}

////////////////////////////////////////////////////
/////////////////// REQUESTS ///////////////////////
////////////////////////////////////////////////////

function requestBarGain(barNum) {
	if (unitCurrentMode != LEARN_MODE) {
		setLearnMode();
	}
	messnamed("barNumForDataRequest", barNum);
	outlet(0, "/g/r/barGain", barNum);
}

function requestNegativeVelocityOffset(barNum) {
	if (unitCurrentMode != LEARN_MODE) {
		setLearnMode();
	}
	messnamed("barNumForDataRequest", barNum);
	outlet(0, "/g/r/negativeVelocityOffset", barNum);
}

function requestBarDownThreshold(barNum) {
	if (unitCurrentMode != NORMAL_MODE) {
		setNormalMode();
	}
	messnamed("barNumForDataRequest", barNum);
	outlet(0, "/g/r/barDownThreshold", barNum);
}

function requestBarUpThreshold(barNum) {
	if (unitCurrentMode != NORMAL_MODE) {
		setNormalMode();
	}
	messnamed("barNumForDataRequest", barNum);
	outlet(0, "/g/r/barUpThreshold", barNum);
}

function requestTare(barNum) {
	if (unitCurrentMode != LEARN_MODE) {
		setLearnMode();
	}
	messnamed("barNumForDataRequest", barNum);
	outlet(0, "/fw/r/tare", barNum);
}

////////////////////////////////////////////////////
////////// REQUEST DATA FOR EVERY BAR //////////////
////////////////////////////////////////////////////

var requestStepIndex = 0;
var isIterativelyRequestingAllBarData = false;


var currentRequestStep;
var barNumForBarBeingUpdated = 0;



function msg_fromMarimba(messageType, msgBarNum) {
	if (!isIterativelyRequestingAllBarData) {
		return;
	}
	//post(messageType, requestSteps[requestStepIndex], msgBarNum, barNumForBarBeingUpdated, "\n");
	
	if (messageType == currentRequestStep && msgBarNum == barNumForBarBeingUpdated) {
		// we got a response back from our last request, so proceed forward	
		barNumForBarBeingUpdated += 1;
		iterativelyRequestAllBarData();
	}
}


function startIterativelyRequestingAllBarData() {
	setLearnMode();
	isIterativelyRequestingAllBarData = true;
	outlet(1, 1);
	requestStepIndex = 0;
	barNumForBarBeingUpdated = 0;
	iterativelyRequestAllBarData();
}

function iterativelyRequestAllBarData() {
	
	// putting requestSteps here rather than a global cuz max likes to load this file first, then
	// spit out an error that it can't find "BAR_GAIN" (defined in a different file)
	var requestSteps = [BAR_GAIN, NEGATIVE_VELOCITY_OFFSET, TARE, BAR_DOWN_THRESHOLD, BAR_UP_THRESHOLD];

	
	if (barNumForBarBeingUpdated > 42) {
		if (requestStepIndex < requestSteps.length-1) {
			requestStepIndex += 1;
			barNumForBarBeingUpdated = 0;
		}

		else { // done updating
			requestStepIndex = 0;
			barNumForBarBeingUpdated = 0;
			isIterativelyRequestingAllBarData = false;
		
			outlet(1, 0);
			return;
		}
	}

	currentRequestStep = requestSteps[requestStepIndex];
	
	switch (currentRequestStep) {
		case BAR_GAIN:
			requestBarGain(barNumForBarBeingUpdated);
			break;
		case NEGATIVE_VELOCITY_OFFSET:
			requestNegativeVelocityOffset(barNumForBarBeingUpdated);
			break;
		case TARE:
			requestTare(barNumForBarBeingUpdated);
			break;
		case BAR_DOWN_THRESHOLD:
			requestBarDownThreshold(barNumForBarBeingUpdated);
			break;
		case BAR_UP_THRESHOLD:
			requestBarUpThreshold(barNumForBarBeingUpdated);
			break;
		default:
			post(currentRequestStep + " isn't implemented\n")
			return;
	}	
}









