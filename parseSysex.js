inlets = 1;
outlets = 5;


post("Loaded sysexToOSC.js script\n");

autowatch = true;

var STATE_WAITING_FOR_START = 0;
var STATE_COLLECTING_BYTES = 1;



function addByteToSysexMessage(byte) {
	switch (state) {
		case STATE_WAITING_FOR_START:
			// post("WAITING FOR START " + "\n");

			if (byte == 0xF0) {
				message[0] = 0xF0;
				receiveIndex = 1;
				state = STATE_COLLECTING_BYTES;
			}
			break;
			
		case STATE_COLLECTING_BYTES:
			message[receiveIndex++] = byte	
			
			if (byte == 0xF7) {	
				encodeOSC(message);
				
				receiveIndex = 0;
				state = STATE_WAITING_FOR_START;
			}
			
			// if we don't get an 0xF7 and we're at the end of the buffer, bail and start over
			if (receiveIndex > 255) {
				receiveIndex = 0;
				state = STATE_WAITING_FOR_START;
			}
			break;
	}
}