

const Max = require('max-api');

var _oscJs = require('osc-js');
var _oscJs2 = _interopRequireDefault(_oscJs);


function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }


Max.post(`Loaded sysexToOSC.js script`);
Max.autowatch = 1;

var STATE_WAITING_FOR_START = 0
var STATE_COLLECTING_BYTES = 1

var state = STATE_WAITING_FOR_START;
var receiveIndex = 0;

var message = new Array(256)

/// When a request is made for a bar's gain, -offset, barDownThresh or barUpThresh in the UI
///  - barNumForDataRequest is set to the barNun
///  - a sysex message is sent out to request the gain/-offset/barDownThresh/barUpThresh
///  - when the requested gain/-offset/barDownThresh/barUpThresh is received, this file 
/// 	 - prepends the gain/-offset/barDownThresh/barUpThresh with the barnum saved in barNumForDataRequest
///		 (the sysex message does not include the barNum, only the gain/-offset/barDownThresh/barUpThresh value)
///      - outlets [barNumForDataRequest, gain/-offset/barDownThresh/barUpThresh value] to be used in the vis
var barNumForDataRequest;

var requestedBarForTare;

//encodeOSC([240, 0, 1, 95, 122, 39, 1, 0, 35, 47, 100, 47, 114, 112, 97, 119, 86, 101, 112, 108, 111, 99, 105, 112, 116, 121, 0, 0, 112, 44, 105, 105, 0, 112, 0, 0, 0, 42, 112, 0, 0, 0, 23, 120, 247])

function encodeOSC(data) {
	// unpack OSC chunks into buffer
	var oscBuffer = decodeSysex(data);
	//Max.post(oscBuffer);
	var oscmsg = new _oscJs2.default.Message();

	oscmsg.unpack(new DataView(oscBuffer.buffer));
	Max.outlet("oscDisplay", oscmsg);
	parseAndOutletOSC(oscmsg);

	//Max.outlet(oscmsg);
} // parse sysex to OSC

function parseAndOutletOSC(msg) {
	switch (msg["address"]) {
		case "/d/rawVelocity":
			Max.outlet("msg_fromMarimba", "RAW_VELOCITY", msg["args"][0], msg["args"][1]);
			break;
		case "/g/w/barGain":
			Max.outlet("msg_fromMarimba", "BAR_GAIN", barNumForDataRequest, msg["args"][0]);
			break;
		case "/g/w/negativeVelocityOffset":
			Max.outlet("msg_fromMarimba", "NEGATIVE_VELOCITY_OFFSET", barNumForDataRequest, msg["args"][0]);
			break;
		case "/fw/w/tare":
			Max.outlet("msg_fromMarimba", "TARE", barNumForDataRequest, msg["args"][0]);
			break;
		case "/g/w/barDownThreshold":
			Max.outlet("msg_fromMarimba", "BAR_DOWN_THRESHOLD", barNumForDataRequest, msg["args"][0]);
			break;
		case "/g/w/barUpThreshold":
			Max.outlet("msg_fromMarimba", "BAR_UP_THRESHOLD", barNumForDataRequest, msg["args"][0]);
			break;
		default:
			Max.outlet("msg_fromMarimba", "unknown_message", msg["args"][0]);
			break;
  }
}

function decodeSysex(message) {
  // Names for bits
  var BIT_00 = 1,
	  BIT_01 = 1 << 1,
	  BIT_02 = 1 << 2,
	  BIT_03 = 1 << 3,
	  BIT_04 = 1 << 4,
	  BIT_05 = 1 << 5,
	  BIT_06 = 1 << 6,
	  BIT_07 = 1 << 7;

  // ignore sysex start, device identity, sysex close, resulting in only message payload
  var sysexPayload = message.slice(9, -1),
	  messageLength = sysexPayload.length;

  var oscBufferIndex = 0,
	  messageBuffer = [];

  for (var i = 0; i < messageLength; i += 5) {
	// 5th byte low nibble contains the high bits of the previous 4 bytes
	var msbits = sysexPayload[i + 4];

	messageBuffer[oscBufferIndex] = sysexPayload[i];

	if (msbits & BIT_00) {
		messageBuffer[oscBufferIndex] |= BIT_07;
	}

	messageBuffer[oscBufferIndex + 1] = sysexPayload[i + 1];

	if (msbits & BIT_01) {
		messageBuffer[oscBufferIndex + 1] |= BIT_07;
	}

	messageBuffer[oscBufferIndex + 2] = sysexPayload[i + 2];

	if (msbits & BIT_02) {
		messageBuffer[oscBufferIndex + 2] |= BIT_07;
	}

	messageBuffer[oscBufferIndex + 3] = sysexPayload[i + 3];

	if (msbits & BIT_03) {
		messageBuffer[oscBufferIndex + 3] |= BIT_07;
	}

	oscBufferIndex += 4;
  }

  return new Uint8Array(messageBuffer);
}

Max.addHandler('setBarNumForDataRequest', (value) => {
	barNumForDataRequest = value;
});



Max.addHandler('addByteToSysexMessage', (byte) => {
	switch (state)
	{
		case STATE_WAITING_FOR_START:
			// post("WAITING FOR START " + "\n");

			if (byte == 0xF0) {
				message[0] = 0xF0;
				receiveIndex = 1;
				state = STATE_COLLECTING_BYTES;
			}
			break;
			
		case STATE_COLLECTING_BYTES:
			message[receiveIndex++] = byte	
			
			if (byte == 0xF7) {	
				encodeOSC(message);
				
				receiveIndex = 0;
				state = STATE_WAITING_FOR_START;
			}
			
			// if we don't get an 0xF7 and we're at the end of the buffer, bail and start over
			if (receiveIndex > 255) {
				receiveIndex = 0;
				state = STATE_WAITING_FOR_START;
			}
			break;
	}
});

